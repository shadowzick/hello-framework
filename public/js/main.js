function handleCallbacks(callbacks) {
    var that = this;
    if(callbacks != null){
       $.each(callbacks, function(i, o) {
          window[o.name].apply(that, o.args);
      });
    }
}